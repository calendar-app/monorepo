## Calendar WebApp for ITS Calendar

### Project Structure:

``` javascript
src
-- frontend  // Angular project
-- calendar  // NodeJS Api
-- scraper   // NodeJS script for continuous scraping the calendar CSV
-- traefik   // Contains files for serving App over the internet enabling HTTPS
```